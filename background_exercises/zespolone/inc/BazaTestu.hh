#ifndef BAZATESTU_HH
#define BAZATESTU_HH


#include "WyrazenieZesp.hh"
#include <fstream>


/*
 * Modeluje pojecie baze testu z zestawem pytan w tablicy
 * oraz informacji o maksymalnej ilosci pytan, jak
 * tez indeksem nastepnego pytania, ktore ma byc pobrane
 * z bazy.
 */
class BazaTestu {
  std::fstream test_file_strm;
public:
  bool open_file(std::string name);
  bool close_file();
  bool next_question(WyrazenieZesp & expr);
  bool end_file();
};

#endif
