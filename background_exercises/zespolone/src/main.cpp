#include <iostream>
#include "LZespolona.hh"
#include "WyrazenieZesp.hh"
#include "BazaTestu.hh"
#include "Statistics.hh"

using namespace std;




int main(int argc, char **argv)
{

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }

  BazaTestu Test;
  if (!Test.open_file(argv[1]))
  {
    cout << "Otwarcie pliku nie powiodło się.";
    return 0;
  }

  
  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_question;
  LZespolona users_input;
  Statistics stats;
  

  
  do{
    cout << "Podaj wynik operacji:  ";
    Test.next_question(WyrZ_question);
    cout << "Twoja odpowiedź: " << endl;
    cin >> users_input;

    int k = 2;
    while (cin.fail()){
      cin.clear();
      cin.ignore(1000, '\n');
      if (k > 0){
        cout << "Pozostałe próby: " <<k<< " ,spróbuj ponownie wpisać odpowiedź ,np. (2+2i) :" << endl;
        cin >> users_input;
        k -= 1;
      } 
    }

    if (users_input == WyrZ_question.calculate()){
      stats.add_correct();
    }
    else{
      stats.add_wrong();
    }
  }while (!Test.end_file());

  cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;

  stats.display_stats();
  cout << "Wynik procentowy poprawnych odpowiedzi:  ";
  stats.display_average(); 
  cout << "%" << endl;

}
