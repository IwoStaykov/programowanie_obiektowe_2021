#include "WyrazenieZesp.hh"
#include "LZespolona.hh"
#include <iostream>

WyrazenieZesp::WyrazenieZesp()
{
    arg1 = {0,0};
    op = op_add;
    arg2 = {0,0};
}

WyrazenieZesp::WyrazenieZesp(LZespolona _arg1, Operator _op, LZespolona _arg2)
{
    arg1 = _arg1;
    op = _op;
    arg2 = _arg2;
}

Operator WyrazenieZesp::get_op() const
{
    return op;
}

void WyrazenieZesp::set_op(Operator _op)
{
    op = _op;
}

LZespolona WyrazenieZesp::get_arg1() const
{
    return arg1;
}

void WyrazenieZesp::set_arg1(LZespolona number)
{
    arg1 = number;
}

LZespolona WyrazenieZesp::get_arg2() const
{
    return arg2;
}

void WyrazenieZesp::set_arg2(LZespolona number)
{
    arg2 = number;
}

LZespolona WyrazenieZesp::calculate()
{
    LZespolona result;

    switch(op)
    {
        case op_add:
            result = arg1 + arg2;
            break;
        case op_subtract:
            result = arg1 - arg2;
            break;
        case op_multiply:
            result = arg1 * arg2;
            break;
        case op_divide:
            result = arg1 / arg2;
            break;
        default:
            break;
    }
    return result;
}

std::ostream & operator << (std::ostream & strm, const WyrazenieZesp & com_expr)
{
    strm << com_expr.get_arg1() << com_expr.get_op() << com_expr.get_arg2();
    return strm;
}

std::ostream & operator << (std::ostream & strm, const Operator & op)
{
    switch(op)
    {
        case op_add:
            strm << '+';
            break;
        case op_subtract:
            strm << '-';
            break;
        case op_multiply:
            strm << '*';
            break;
        case op_divide:
            strm << '/';
            break;
        default:
            break;
    }
    return strm;
}

std::istream & operator >> (std::istream & strm, WyrazenieZesp & com_expr)
{
    LZespolona num1, num2;
    Operator op;
    strm >> num1 >> op >> num2;
    com_expr.set_arg1(num1);
    com_expr.set_op(op);
    com_expr.set_arg2(num2);
    return strm;
}

std::istream & operator >> (std::istream & strm, Operator & op)
{
    char input;
    strm >> input;
    switch(input)
    {
        case '+':
            op = op_add;
            break;
        case '-':
            op = op_subtract;
            break;
        case '*':
            op = op_multiply;
            break;
        case '/':
            op = op_divide;
            break;
        default:
            break;
    }
    return strm;
}

void WyrazenieZesp::display() const
{
    std::cout << arg1 << op << arg2 << std::endl;
}