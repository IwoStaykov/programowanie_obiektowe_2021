#include "LZespolona.hh"
#include <cmath>
#include <iostream>
#include <istream>
#include <ostream>
#include <cctype>

LZespolona::LZespolona(double _re, double _im)
{
  re = _re;
  im = _im;
}
LZespolona::LZespolona()
{
  re = 0;
  im = 0;
}

const LZespolona LZespolona::operator + (const LZespolona num2)
{
  LZespolona  result;

  result.re = re + num2.re;
  result.im = im + num2.im;
  return result;
}

const LZespolona LZespolona::operator - (const LZespolona num2)
{
  LZespolona result;

  result.re = re - num2.re;
  result.im = im - num2.im;
  return result;
}

const LZespolona LZespolona::operator * (const LZespolona num2)
{
  LZespolona result;

  result.re = (re * num2.re) - (im * num2.im);
  result.im = (re * num2.im) + (im * num2.re);
  return result;
}

double LZespolona::modulus_p() const//kwadrat modułu liczby zespolonej
{
  double sum = pow(re, 2) + pow(im, 2); //suma kwadratów
  return sum;
}

LZespolona LZespolona::conjugate() const//sprzężenie liczby zespolonej
{
  LZespolona con_num;

  con_num.re = re;
  con_num.im = -im;
  return con_num;
}

const LZespolona LZespolona::operator / (const LZespolona num2)
{
  LZespolona result;
  LZespolona con_num2 = num2.conjugate(); //sprzężenie
  double mod = num2.modulus_p(); //moduł

  result.re = (re * con_num2.re) - (im * con_num2.im); //pomnożenie l zespolonej i sprzężenia
  result.im = (re * con_num2.im) + (im * con_num2.re);

  result.re = result.re/mod;//podzielenie odp składowej przez kwadrat modułu
  result.im = result.im/mod;
  return result;
}

int LZespolona::operator == (LZespolona num2) const
{
  if (re == num2.re && im == num2.im){
    return 1;//gdy liczby są równe
  }
  else{
    return 0;//gdy liczby są różne
  }
}

double LZespolona::get_re() const
{
  return re;
}

double LZespolona::get_im() const
{
  return im;
}

void LZespolona::set_re(double _re)
{
  re = _re;
}

void LZespolona::set_im(double _im)
{
  im = _im;
}

std::ostream & operator << (std::ostream & strm, const LZespolona & num)
{
  strm<<"("<<num.get_re()<<std::showpos<<num.get_im()<<std::noshowpos<<"i)";
  return strm;
}
std::istream & operator >> (std::istream & strm, LZespolona & num)
{
  char input;
  double number;
  strm >> input;
  if (input != '(')
  {
    strm.setstate(std::ios_base::failbit);
    return strm;
  }
  strm >> number;
  if (strm.fail())
  {
    strm.setstate(std::ios_base::failbit);
    return strm;
  }
  else
  {
    num.set_re(number);
  }
  strm >> number;
  if (strm.fail())
  {
    strm.setstate(std::ios_base::failbit);
    return strm;
  }
  else
  {
    num.set_im(number);
  }
  strm >> input;
  if (input != 'i')
  {
    strm.setstate(std::ios_base::failbit);
    return strm;
  }
  strm >> input;
  if (input != ')')
  {
    strm.setstate(std::ios_base::failbit);
    return strm;
  }
  if(strm.fail()){
    std::cerr << "Podano zły format liczby zespolonej" << std::endl;
  }
  return strm;
}