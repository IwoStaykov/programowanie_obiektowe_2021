#include <iostream>
#include <vector>
#include "Wektor2D.hh"
#include "MacierzRotacji.hh"
#include "Prostokat.hh"
#include "Dr2D_gnuplot_api.hh"

void display_menu(char *p_choice, int mode=0){
    using namespace std;
    cout << "=========================ROTACJE 2D=========================" << endl;
    cout << "=  o -> obrót prostokąta o dany kąt                        =" << endl;
    cout << "=  p -> przesunięcie prostokąta o dany wektor              =" << endl;
    cout << "=  w -> wyświetlenie współrzędnych wierzchołków prostokąta =" << endl;
    cout << "=  m -> wyświetlenie menu                                  =" << endl;
    cout << "=  k -> wyjście z programu                                 =" << endl;
    cout << "============================================================" << endl;
    if(mode == 0){
        cout << " Wybór opcji: ";
        cin >> p_choice;
    }
}

int main(){
    char choice;
    char *p_choice = &choice;
    double angle_degrees;
    long int rotation_repeat;
    Wektor2D ld(1,1);
    Wektor2D lg(1,2);
    Wektor2D pg(3,2);
    Wektor2D pd(3,1);
    Rectangle rect(lg,pg,ld,pd);
    Wektor2D moving_vector;

    display_menu(p_choice,1);
    drawNS::Draw2DAPI *drafter = new drawNS::APIGnuPlot2D(-10,10,-10,10,1000);
    rect.draw(drafter);
    while (choice != 'k'){
        rect.equal_sides();
        std::cout << "Wybór opcji: ";
        std::cin >> choice;
        switch(choice){
            case 'o':
                std::cout << "Wprowadź kąt obrotu: ";
                std::cin >> angle_degrees;
                std::cout << "Wprowadź liczbę obrotów: ";
                std::cin >> rotation_repeat;
                if(rotation_repeat == 0)
                    break;
                rect.rotate(angle_degrees*rotation_repeat);
                rect.draw(drafter);
                break;
            case 'p':
                std::cout << "Wprowadź wektor przesunięcia: ";
                std::cin >> moving_vector;
                rect.move(moving_vector);
                rect.draw(drafter);
                break;
            case 'w':
                std::cout << "Współrzędne wierzchołków: " << std::endl;
                std::cout << rect;
                break;
            case 'm':
                display_menu(p_choice);
            case 'k':
                break;
        }
    }
    delete drafter;
}