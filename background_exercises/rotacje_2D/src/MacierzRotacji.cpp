#include "MacierzRotacji.hh"
#include "Wektor2D.hh"
#include <vector>
#include <iostream>
#include <cmath>

#define PI 3.14159265359

const Wektor2D &MatrixRot::operator [] (int index) const{
    if (index != 0 && index != 1){
        std::cerr << "Nieprawidłowy indeks macierzy" << std::endl;
    }
    return row[index];
}
MatrixRot::MatrixRot(){
    Wektor2D first_row(0,0);
    Wektor2D second_row(0,0);
    row.push_back(first_row);
    row.push_back(second_row);
}
/* Przedstawiamy macierz obrotu jako dwa wektory wierszy */
MatrixRot::MatrixRot(double angle){
    double radians = angle * PI / 180;
    Wektor2D first_row(cos(radians), -sin(radians));
    Wektor2D second_row(sin(radians), cos(radians));
    row.push_back(first_row);
    row.push_back(second_row);
}

MatrixRot MatrixRot::operator * (const MatrixRot &arg) const{
    MatrixRot result;

    Wektor2D first_row(row[0][0]*arg[0][0] + row[0][1]*arg[1][0],row[0][0]*arg[0][1] + row[0][1]*arg[1][1]);
    Wektor2D second_row(row[1][0]*arg[0][0] + row[1][1]*arg[1][0],row[1][0]*arg[0][1] + row[1][1]*arg[1][1]);
    result.row[0] = first_row;
    result.row[1] = second_row;
    return result;

}

Wektor2D MatrixRot::operator * (const Wektor2D &point) const{
    Wektor2D result;    

    result[0] = row[0][0]*point[0] + row[0][1]*point[1];
    result[1] = row[1][0]*point[0] + row[1][1]*point[1];
    return result;
}

std::ostream& operator << (std::ostream &strm, const MatrixRot &Matrix){
    strm<<'['<<Matrix[0][0]<<','<<Matrix[0][1]<<']'<<std::endl;
    strm<<'['<<Matrix[1][0]<<','<<Matrix[1][1]<<']'<<std::endl;
    return strm;
}