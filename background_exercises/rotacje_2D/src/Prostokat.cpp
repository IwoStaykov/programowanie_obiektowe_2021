#include "Prostokat.hh"
#include "Wektor2D.hh"
#include "MacierzRotacji.hh"
#include "Dr2D_gnuplot_api.hh"
#include <cmath>
#include <vector>

#define EPSILON 0.000000000000000000000000000000000000000000000000000001

Rectangle::Rectangle(){
    Wektor2D up_l(0,1);
    Wektor2D up_r(1,1);
    Wektor2D down_l(0,0);
    Wektor2D down_r(1,0);

    rect_vect.push_back(up_l);
    rect_vect.push_back(up_r);
    rect_vect.push_back(down_l);
    rect_vect.push_back(down_r);
}

Rectangle::Rectangle(Wektor2D up_l, Wektor2D up_r, Wektor2D down_l, Wektor2D down_r){
    double len_left = (up_l - down_l).length();
    double len_right = (up_r - down_r).length();
    double len_up = (up_r - up_l).length();
    double len_down = (down_r - down_l).length();

    if(abs(len_left - len_right) > EPSILON || abs(len_up - len_down) > EPSILON){
        std::cerr << "Nierówne boki"<<std::endl;
    }
    if(abs((up_r - up_l)*(up_r - down_r)) > EPSILON){
        std::cerr << "Iloczyn skalarny różny od zera" << std::endl;
    }

    rect_vect.push_back(down_l);
    rect_vect.push_back(up_l);
    rect_vect.push_back(up_r);
    rect_vect.push_back(down_r);
}

void Rectangle::move(const Wektor2D &arg){
    for(int i=0; i < 4; ++i){
        rect_vect[i] = rect_vect[i] + arg;
    }
}

void Rectangle::rotate(double angle_degrees){
    MatrixRot matrix(angle_degrees);

    for(int i=0; i<4; ++i){
        rect_vect[i] = matrix * rect_vect[i];
    }
}

void Rectangle::draw(drawNS::Draw2DAPI *drafter/* , std::string colour="black" */){
    std::vector<drawNS::Point2D> points;
    points.push_back(convert(rect_vect[0]));
    points.push_back(convert(rect_vect[1]));
    points.push_back(convert(rect_vect[2]));
    points.push_back(convert(rect_vect[3]));
    points.push_back(convert(rect_vect[0]));
    shape_id = drafter->draw_polygonal_chain(points);
}

void Rectangle::equal_sides(){
    double len_left = (rect_vect[0] - rect_vect[1]).length();
    double len_right = (rect_vect[2] - rect_vect[3]).length();
    double len_up = (rect_vect[1] - rect_vect[2]).length();
    double len_down = (rect_vect[0] - rect_vect[3]).length();

    if(abs(len_left - len_right) > EPSILON || abs(len_up - len_down) > EPSILON){
        std::cerr << "Nierówne boki"<<std::endl;
    }
    else{
        std::cout <<std::endl<< "Boki są równe" <<std::endl;
        std::cout << "Lewy bok: " << len_left << std::endl;
        std::cout << "Prawy bok: " << len_right << std::endl;
        std::cout << "Górny bok: " << len_up << std::endl;
        std::cout << "Dolny bok: " << len_down << std::endl;
    }
}

const Wektor2D &Rectangle::operator [] (double index) const{
    if (index != 0 && index != 1 && index != 2 && index != 3){
        std::cerr << "Odwołanie poza prostokąt" << std::endl;
    }
    return rect_vect[index];
}

std::ostream &operator << (std::ostream &strm, const Rectangle &rect){
    strm << "Wierzchołek 1  " << rect[0] << std::endl;
    strm << "Wierzchołek 2  " << rect[1] << std::endl;
    strm << "Wierzchołek 3  " << rect[2] << std::endl;
    strm << "Wierzchołek 4  " << rect[3] << std::endl;
    return strm;
}