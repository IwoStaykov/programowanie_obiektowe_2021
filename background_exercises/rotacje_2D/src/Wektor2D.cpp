#include "Wektor2D.hh"
#include "Dr2D_gnuplot_api.hh"
#include <iostream>
#include <vector>
#include <cmath>

const double &Wektor2D::operator [] (int index) const
{
    if (index != 0 && index != 1){
        std::cerr << "Nieprawidłowy indeks wektora" << std::endl;
    }
    return xy[index];
}

double &Wektor2D::operator [] (int index)
{
    if (index == 0){
        auto pointer_x = xy.begin();
        return *pointer_x;
    }
    else if (index == 1){
        auto pointer_y = xy.end() - 1;
        return *pointer_y;
    }
    else{
        std::cerr << "Nieprawidłowy indeks wektora" << std::endl;
    }
}

Wektor2D::Wektor2D(){
    xy.push_back(0);
    xy.push_back(0);
}

Wektor2D::Wektor2D(double _x, double _y){
    xy.push_back(_x);
    xy.push_back(_y);
}

const Wektor2D Wektor2D::operator + (const Wektor2D &second_vect) const{
    Wektor2D result;

    result[0] = xy[0] + second_vect[0];
    result[1] = xy[1] + second_vect[1];
    return result;
}

const Wektor2D Wektor2D::operator - (const Wektor2D &second_vect) const{
    Wektor2D result;

    result[0] = xy[0] - second_vect[0];
    result[1] = xy[1] - second_vect[1];
    return result;
}

const Wektor2D Wektor2D::operator * (const double scalar) const{
    Wektor2D result;

    result[0] = scalar * xy[0];
    result[1] = scalar * xy[1];
    return result;
}

const Wektor2D Wektor2D::operator / (const double scalar) const{
    Wektor2D result;

    result[0] = xy[0] / scalar;
    result[1] = xy[1] / scalar;
    return result;
}

const double Wektor2D::operator * (const Wektor2D &second_vect) const{
    double result;

    result = xy[0] * second_vect[0] + xy[1] * second_vect[1];
    return result;
}

double Wektor2D::length() const{
    double result;

    result = sqrt(pow(xy[0],2) + pow(xy[1],2));
    return result;
}

drawNS::Point2D  convert(const Wektor2D &arg){
    return drawNS::Point2D(arg[0],arg[1]);
}

std::istream &operator >> (std::istream &strm, Wektor2D &vect){
    double number;
    char comma;
    strm >> number;
    if (strm.fail()){
        strm.setstate(std::ios_base::failbit);
        return strm;
    }
    else{
        vect[0] = number;
    }
    strm >> comma;
    if (comma != ','){
        strm.setstate(std::ios_base::failbit);
        return strm;
    }
    strm >> number;
    if (strm.fail()){
        strm.setstate(std::ios_base::failbit);
        return strm;
    }
    else{
        vect[1] = number;
    }
    return strm;
}

std::ostream &operator << (std::ostream &strm, const Wektor2D &vect){
    strm <<'['<< vect[0] << ',' << vect[1] <<']'<< std::endl;
    return strm;
}
