#ifndef WEKTOR2D_HH
#define WEKTOR2D_HH

#include <iostream>
#include <vector>
#include "Dr2D_gnuplot_api.hh"

class Wektor2D {
  std::vector<double> xy;
public:
  //przeciążenie indeksów
  const double & operator [] (int index) const; //pobranie wartości
  double & operator [] (int index); //zmiana wartości

  //konstruktory
  Wektor2D();
  Wektor2D(double _x, double _y);

  //przeciążenia operatorów do podst operacji na wektorach
  const Wektor2D operator + (const Wektor2D & second_vect) const;
  const Wektor2D operator - (const Wektor2D & second_vect) const;
  const Wektor2D operator * (const double scalar) const;
  const Wektor2D operator / (const double scalar) const;
  //iloczyn skalarny
  const double operator * (const Wektor2D & second_vect) const;
  double length() const;
};

drawNS::Point2D convert(const Wektor2D & arg);
std::istream& operator >> (std::istream &strm, Wektor2D &Wek);
std::ostream& operator << (std::ostream &strm, const Wektor2D &Wek);

#endif
