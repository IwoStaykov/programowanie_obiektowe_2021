#ifndef MACIERZROTACJI_HH
#define MACIERZROTACJI_HH

#include <vector>
#include <iostream>
#include <cmath>
#include "Wektor2D.hh"

class MatrixRot {
  std::vector<Wektor2D> row;
public:
  MatrixRot();
  MatrixRot(double angle);

  MatrixRot operator * (const MatrixRot &arg) const;
  Wektor2D operator * (const Wektor2D &point) const;
  const Wektor2D &operator [] (int index) const;
};

std::ostream& operator << (std::ostream &strm, const MatrixRot &Matrix);

#endif
