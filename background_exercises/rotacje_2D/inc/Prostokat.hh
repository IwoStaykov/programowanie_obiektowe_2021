#ifndef PROSTOKAT_HH
#define PROSTOKAT_HH

#include <iostream>
#include <vector>
#include "Wektor2D.hh"
#include "MacierzRotacji.hh"
#include "Dr2D_gnuplot_api.hh"
#include "Draw2D_api_interface.hh"

class Rectangle {
  std::vector<Wektor2D> rect_vect;
  int shape_id;
public:
  Rectangle();
  Rectangle(Wektor2D side_l, Wektor2D side_r, Wektor2D up, Wektor2D down);
  void move(const Wektor2D &arg);
  void rotate(double angle_degrees);
  void draw(drawNS::Draw2DAPI *drafter/* , std::string colour="black" */);
  void equal_sides();
  const Wektor2D &operator [] (double index) const;
};

std::ostream &operator << (std::ostream &strm, const Rectangle &rect);


#endif
