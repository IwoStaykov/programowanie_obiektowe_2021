#ifndef STATISTICS_HH
#define STATISTICS_HH

#include <ostream>

class Statistics{
    float correct;
    float wrong;
public:
    Statistics();
    void add_correct();
    void add_wrong();
    void display_average();
    void display_stats();

};


#endif