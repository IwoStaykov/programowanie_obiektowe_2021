#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH



#include "LZespolona.hh"
enum Operator { op_add, op_subtract, op_multiply, op_divide }; //modeluje operacje matematyczne
class WyrazenieZesp {
  LZespolona   arg1;   // Pierwszy argument wyrazenia arytmetycznego
  Operator     op;     // Opertor wyrazenia arytmetycznego
  LZespolona   arg2;   // Drugi argument wyrazenia arytmetycznego
public:
  WyrazenieZesp();
  WyrazenieZesp(LZespolona _arg1, Operator _op, LZespolona _arg2);
  Operator get_op() const;
  void set_op(Operator _op);
  LZespolona get_arg1() const;
  LZespolona get_arg2() const;
  void set_arg1(LZespolona number);
  void set_arg2(LZespolona number);
  LZespolona calculate();
  void display() const;
};

  std::ostream & operator << (std::ostream & strm, const WyrazenieZesp & com_expr);
  std::ostream & operator << (std::ostream & strm, const Operator & op);
  std::istream & operator >> (std::istream & strm, WyrazenieZesp & com_expr);
  std::istream & operator >> (std::istream & strm, Operator & op);                 
#endif
