#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH


#include <istream>
#include <ostream>
/*!
 *  Plik zawiera definicje struktury LZesplona oraz zapowiedzi
 *  przeciazen operatorow arytmetycznych dzialajacych na tej 
 *  strukturze.
 */


/*!
 * Modeluje pojecie liczby zespolonej
 */
class  LZespolona {

  double   re;    /*! Pole repezentuje czesc rzeczywista. */
  double   im;    /*! Pole repezentuje czesc urojona. */

public:
  double modulus_p() const;
  LZespolona conjugate() const;
  const LZespolona operator + (const LZespolona num2);
  const LZespolona operator - (const LZespolona num2);
  const LZespolona operator * (const LZespolona num2);
  const LZespolona operator / (const LZespolona num2);
  int operator == (LZespolona num2) const;

  LZespolona(double _re, double _im);
  LZespolona();
  double get_re() const;
  double get_im() const;
  void set_re(double _re);
  void set_im(double _im);
};

std::ostream & operator << (std::ostream & strm, const LZespolona & num);

std::istream & operator >> (std::istream & strm, LZespolona & num);




#endif
