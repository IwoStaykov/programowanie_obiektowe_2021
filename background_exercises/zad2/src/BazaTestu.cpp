#include <iostream>
#include <cstring>
#include <cassert>
#include "BazaTestu.hh"
#include <fstream>

using namespace std;

bool BazaTestu::open_file(string name)
{
    test_file_strm.open(name, fstream::in);
    if (test_file_strm)
    {
        cout << "Plik z pytaniami otwarty" << endl;
        return true;
    }
    return false;
}

bool BazaTestu::close_file()
{
    test_file_strm.close();
    cout << "Plik z pytaniami zamknięty" << endl;
    return true;
}

bool BazaTestu::next_question(WyrazenieZesp & expr)
{
    test_file_strm >> expr;
    if (test_file_strm.good())
    {
        cout << expr << endl;
        return true;
    }
    else
    {
        if(!test_file_strm.eof())
        cout << "Błąd wczytania z pliku" << endl;
        return false;
    }
}

bool BazaTestu::end_file()
{
    if(test_file_strm.eof())
    {
        return true; //koniec pliku
    }
    else
        return false; //są jeszcze pytania

}

