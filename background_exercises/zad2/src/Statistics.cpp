#include "Statistics.hh"
#include <iostream>
#include <ostream>

Statistics::Statistics()
{
    correct = 0;
    wrong = 0;
}

void Statistics::add_correct()
{
    correct += 1;
}

void Statistics::add_wrong()
{
    wrong += 1;
}
void Statistics::display_average()
{
    float average;
    average = correct/(correct+wrong);
    std::cout << average;
}

void Statistics::display_stats()
{
    std::cout << "Ilość poprawnych odpowiedzi:  " << correct;
    std::cout << "Ilość niepoprawnych odpowiedzi:  " << wrong;
}