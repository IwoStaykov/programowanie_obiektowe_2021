#include <iostream>
#include <vector>
#include <cmath>
#include "Wektor.hh"
#include "MacierzRotacji.hh"
#include "Prostopadloscian.hh"

#define NUM_APEXES 8
#define EPSILON 0.000000000000000000000000000000000000000000000000000000001

Wektor<3> Figure::operator[] (int index) const{
    if (index < 0 || index >= NUM_APEXES){
        std::cerr << "Nieprawidłowy indeks figury" << std::endl;
    }
    return coordinates[index];
}

Figure::Figure(){
    Wektor<3> content{0,0,0};
    for(int iter=0; iter < NUM_APEXES; ++iter){
        coordinates.push_back(content);
    }
}

Figure::Figure(Wektor<3> middle_apex, Wektor<3> x_apex, Wektor<3> y_apex, Wektor<3> z_apex){
    /*Wektory przesunięcia z middle_appex do każdego z wierzchołków*
    * Potrzebne do wyprowadzenia pozostałych wierzchołków figury   */    
    Wektor<3> axis_z_vector = z_apex - middle_apex;
    Wektor<3> axis_x_vector = x_apex - middle_apex;
    Wektor<3> axis_y_vector = y_apex - middle_apex;

    /*Sprawdzenie czy boki są wzajemnie prostopadłe*/
    if(axis_x_vector * axis_y_vector > EPSILON || axis_y_vector * axis_z_vector > EPSILON || axis_z_vector * axis_x_vector > EPSILON){
        std::cerr << "To nie jest prostopadłościan (sąsiednie boki nieprostopadłe)" << std::endl;
    }

    /*Pozostałe wierzchołki figury*/
    Wektor<3> z_prim = z_apex + axis_y_vector + axis_x_vector;
    Wektor<3> y_prim = y_apex + axis_z_vector;
    Wektor<3> x_prim = x_apex + axis_z_vector;
    Wektor<3> middle_prim = z_prim - axis_z_vector;

    /*Do łatwiejszego uzupełnienia pól*/
    std::vector<Wektor<3>> content{middle_apex, x_apex, middle_prim, y_apex, z_apex, x_prim, z_prim, y_prim};

    for (int iter=0; iter < NUM_APEXES; ++iter){
        coordinates.push_back(content[iter]);
    }
}

void Figure::move(const Wektor<3> &vect){
    for (int iter=0; iter<NUM_APEXES; ++iter){
        coordinates[iter] = coordinates[iter] + vect;
    }
}

void Figure::rotate(const double &angle_degrees, const char &rotation_axis){
    MatrixRot<3> matrix(angle_degrees, rotation_axis);

    for (int iter=0; iter < NUM_APEXES; ++iter){
        coordinates[iter] = matrix * coordinates[iter];
    }
}

void Figure::draw(drawNS::Draw3DAPI *drafter, std::string colour){
    std::vector<std::vector<drawNS::Point3D>> points; //współrzędne figury do przekazania funkcji draw_polyhedron
    std::vector<drawNS::Point3D> first_base;
    std::vector<drawNS::Point3D> second_base;

    //Pierwsza podstawa
    for (int iter=0; iter < 4; ++iter){
        first_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(first_base);

    //Druga podstawa
    for (int iter=4; iter < 8; ++iter){
    second_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(second_base);

    figure_id = drafter->draw_polyhedron(points, colour);
}

void Figure::erase(drawNS::Draw3DAPI *drafter){
    drafter->erase_shape(figure_id);
}

const void Figure::equal_sides(int output_info) const{
    int error_info=0;
    //wyznaczenie wektorów boków
    Wektor<3> side_01 = coordinates[1] - coordinates[0];
    Wektor<3> side_32 = coordinates[2] - coordinates[3];
    Wektor<3> side_45 = coordinates[5] - coordinates[4];
    Wektor<3> side_76 = coordinates[6] - coordinates[7];

    Wektor<3> side_04 = coordinates[4] - coordinates[0];
    Wektor<3> side_51 = coordinates[5] - coordinates[1];
    Wektor<3> side_62 = coordinates[6] - coordinates[2];
    Wektor<3> side_73 = coordinates[7] - coordinates[3];

    Wektor<3> side_30 = coordinates[3] - coordinates[0];
    Wektor<3> side_21 = coordinates[2] - coordinates[1];
    Wektor<3> side_65 = coordinates[6] - coordinates[5];
    Wektor<3> side_74 = coordinates[7] - coordinates[4];

    if(abs(side_01.length() - side_32.length()) > EPSILON || abs(side_45.length() - side_76.length()) > EPSILON){
        error_info = 1;
    }
    if(abs(side_04.length() - side_51.length()) > EPSILON || abs(side_62.length() - side_73.length()) > EPSILON){
        error_info = 1;
    }
    if(abs(side_30.length() - side_21.length()) > EPSILON || abs(side_65.length() - side_74.length()) > EPSILON){
        error_info = 1;
    }

    if(error_info != 0){
        std::cerr << "Nierówne boki prostopadłościanu!" << std::endl;
    }

    if (output_info != 0){
        std::cout << "Pierwsza czwórka boków:" << std::endl;
        std::cout << side_01.length() << " , " << side_32.length() << std::endl;
        std::cout << side_45.length() << " , " << side_76.length() << std::endl;
        std::cout << "Druga czwórka boków:" << std::endl;
        std::cout << side_04.length() << " , " << side_51.length() << std::endl;
        std::cout << side_62.length() << " , " << side_73.length() << std::endl;
        std::cout << "Trzecia czwórka boków:" << std::endl;
        std::cout << side_30.length() << " , " << side_21.length() << std::endl;
        std::cout << side_65.length() << " , " << side_74.length() << std::endl;
    }
}

std::ostream & operator << (std::ostream & strm, const Figure &cube){
    for(int iter=0; iter < NUM_APEXES; ++iter){
        strm << cube[iter] << std::endl;
    }
    return strm;
}