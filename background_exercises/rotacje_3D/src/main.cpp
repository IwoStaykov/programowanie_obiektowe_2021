#include <iostream>
#include <vector>
#include <cmath>
#include "Wektor.hh"
#include "MacierzRotacji.hh"
#include "Prostopadloscian.hh"
#include "Dr3D_gnuplot_api.hh"
#include "main.hh"

void display_menu(char *p_choice, int mode=0){
    using namespace std;
    cout << "=========================ROTACJE 3D=========================" << endl;
    cout << "=  o -> obrót figury o zadaną sekwencję kątów              =" << endl;
    cout << "=  t -> powtórz poprzedni obrót                            =" << endl;
    cout << "=  p -> przesunięcie figury o dany wektor                  =" << endl;
    cout << "=  r -> wyświetlenie macierzy rotacji                      =" << endl;
    cout << "=  w -> wyświetlenie współrzędnych wierzchołków            =" << endl;
    cout << "=  m -> wyświetl menu                                      =" << endl;
    cout << "=  s -> sprawdzenie długości przeciwległych boków          =" << endl;
    cout << "=  k -> wyjście z programu                                 =" << endl;
    cout << "============================================================" << endl;
    if(mode == 0){
        cout << " Wybór opcji: ";
        cin >> p_choice;
    }
}

//czyszczenie struktury przed następną sekwencją obrotów
void erase_rot_seq(struct RotationSequence & rotation_memory){
    rotation_memory.rotation_axis.erase(rotation_memory.rotation_axis.begin(), rotation_memory.rotation_axis.end());
    rotation_memory.degrees.erase(rotation_memory.degrees.begin(), rotation_memory.degrees.end());
}

//Do wyświetlenia macierzy rotacji
MatrixRot<3> make_matrix(double angle_degrees, char rotation_axis){
    MatrixRot<3> matrix(angle_degrees, rotation_axis);
    return matrix;
}

int main(){
    char input;
    char axis;
    double angle_degrees;
    char choice;
    char *p_choice = &choice;
    long int rotation_repeat;
    Wektor<3> moving_vector;
    struct RotationSequence rotation_memory;
    Wektor<3> middle{0,0,0};
    Wektor<3> x{4,0,0};
    Wektor<3> y{0,3,0};
    Wektor<3> z{0,0,3};
    Figure cuboid(middle, x, y, z);


    display_menu(p_choice,1);
    drawNS::Draw3DAPI *drafter = new drawNS::APIGnuPlot3D(-10,10,-10,10,-10,10,1000);
    cuboid.draw(drafter);
    while (choice != 'k'){
        cuboid.equal_sides(0);
        std::cout << "Wybór opcji: ";
        std::cin >> choice;
        switch(choice){
            case 'o':
                erase_rot_seq(rotation_memory);
                std::cout << "Wprowadź sekwencję oznaczeń osi i kątów rotacji w stopniach np. x 30 : ";
                while (1){
                    std::cin >> input;
                    if (input == '.'){
                        break;
                    }
                    if (input != '.'){
                        axis = input;
                    }
                    if (input != 'z' && input != 'x' && input != 'y'){
                        std::cerr << "Wprowadzono niepoprawną oś, poprawne osie to x, y lub z. Spróbuj ponownie:" << std::endl;
                        continue;
                    }
                    rotation_memory.rotation_axis.push_back(input);
                    std::cin >> angle_degrees;
                    rotation_memory.degrees.push_back(angle_degrees);
                    input = '0';
                }
                std::cout << "Wprowadź liczbę powtórzeń:" << std::endl;
                std::cin >> rotation_repeat;
                if (rotation_repeat != 0){
                    for(int iter = 0; iter < rotation_repeat; ++iter){
                        int degrees_iter = 0;
                        for(char axis : rotation_memory.rotation_axis){
                            cuboid.rotate(rotation_memory.degrees[degrees_iter], axis);
                            ++degrees_iter;
                        }
                    }
                }
                cuboid.erase(drafter);
                cuboid.draw(drafter);
                std::cin.clear();
                break;
            case 't':
                if (rotation_repeat != 0){
                    for(int iter = 0; iter < rotation_repeat; ++iter){
                        int degrees_iter = 0;
                        for(char axis : rotation_memory.rotation_axis){
                            cuboid.rotate(rotation_memory.degrees[degrees_iter], axis);
                            ++degrees_iter;
                        }
                    }
                }
                cuboid.erase(drafter);
                cuboid.draw(drafter);
                break;
            case 'p':
                std::cout << "Wprowadź wektor przesunięcia: ";
                std::cin >> moving_vector;
                cuboid.move(moving_vector);
                cuboid.erase(drafter);
                cuboid.draw(drafter);
                break;
            case 'r':
                std::cout << "Macierz rotacji:" << std::endl;
                std::cout << make_matrix(angle_degrees, axis) << std::endl;
                break;
            case 'w':
                std::cout << "Współrzędne wierzchołków: " << std::endl;
                std::cout << cuboid;
                break;
            case 'm':
                display_menu(p_choice);
                break;
            case 's':
                cuboid.equal_sides(1);
                break;
            case 'k':
                break;
        }
    }
    delete drafter;
}