#include "Wektor.hh"
#include "Dr3D_gnuplot_api.hh"
#include <iostream>
#include <vector>
#include <cmath>

template <int SIZE>
const double &Wektor<SIZE>::operator [] (int index) const
{
    if (index < 0 || index >= SIZE){
        std::cerr << "Nieprawidłowy indeks wektora" << std::endl;
    }
    return vect[index];
}

template <int SIZE>
double &Wektor<SIZE>::operator [] (int index)
{
    if (index < 0 || index >= SIZE){
        std::cerr << "Nieprawidłowy indeks wektora" << std::endl;
    }    
    return vect[index];

}
template <int SIZE>
Wektor<SIZE>::Wektor(){
    for (int i=0; i<SIZE; ++i){
        vect.push_back(0.0);
    }
}

template <int SIZE>
Wektor<SIZE> Wektor<SIZE>::operator + (const Wektor<SIZE> &second_vect) const{
    Wektor<SIZE> result;

    for (int i=0; i<SIZE; ++i){
        result[i] = vect[i] + second_vect[i];
    }
    return result;
}

template <int SIZE>
Wektor<SIZE> Wektor<SIZE>::operator - (const Wektor<SIZE> &second_vect) const{
    Wektor<SIZE> result;

    for (int i=0; i<SIZE; ++i){
        result[i] = vect[i] - second_vect[i];
    }
    return result;
}

template <int SIZE>
Wektor<SIZE> Wektor<SIZE>::operator * (const double scalar) const{
    Wektor<SIZE> result;

    for (int i=0; i<SIZE; ++i){
        result[i] = scalar * vect[i];
    }
    return result;
}

template <int SIZE>
Wektor<SIZE> Wektor<SIZE>::operator / (const double scalar) const{
    Wektor<SIZE> result;

    for (int i=0; i<SIZE; ++i){
        result[i] = vect[i] / scalar;
    }
    return result;
}

template <int SIZE>
const double Wektor<SIZE>::operator * (const Wektor<SIZE> &second_vect) const{
    double result;

    for (int i=0; i<SIZE; ++i){
        result += vect[i] *second_vect[i];
    }
    return result;
}

template <int SIZE>
double Wektor<SIZE>::length() const{
    double result;

    for (int i=0; i<SIZE; ++i){
        result += pow(vect[i],2);
    }
    result = sqrt(result);
    return result;
}

template <int SIZE>
drawNS::Point3D convert(const Wektor<SIZE> &arg){
    return drawNS::Point3D(arg[0],arg[1],arg[2]);
}

/*****************************************************************
 * Wektor wprowadzamy bez nawiasów kwadratowych, odzielając      *
 * współrzędne przecinkami, np. 4.7,6.8 dla wektora [4.7,6.8]    *
 * **************************************************************/
template <int SIZE>
std::istream &operator >> (std::istream &strm, Wektor<SIZE> &vect){
    double number;
    char comma;

    for (int i=0; i<SIZE; ++i){
        strm >> number;
        if (strm.fail()){
            strm.setstate(std::ios_base::failbit);
            return strm;
        }
        else{
            vect[i] = number;
        }
        if (i == SIZE -1){ //aby program nie oczekiwał przecinka po ostatniej współrzędnej wektora
            return strm;
        }
        strm >> comma;
        if (comma != ','){
            strm.setstate(std::ios_base::failbit);
            return strm;
        }
    }
    return strm;
}

template <int SIZE>
std::ostream &operator << (std::ostream &strm, const Wektor<SIZE> &vect){
    strm << "[";
    for (int i=0; i<SIZE; ++i){
        strm << vect[i];
        if (i < SIZE - 1){ //aby nie wyświetlać przecinka po ostatniej współrzędnej
            strm << " , ";
        }
    }
    strm << "]";
    return strm;
}

template class Wektor<2>;
template class Wektor<3>;
template class Wektor<6>;
template class Wektor<8>;

template std::istream& operator >> (std::istream &strm, Wektor<2> &Wek);
template std::ostream& operator << (std::ostream &strm, const Wektor<2> &Wek);

template std::istream& operator >> (std::istream &strm, Wektor<3> &Wek);
template std::ostream& operator << (std::ostream &strm, const Wektor<3> &Wek);

template std::istream& operator >> (std::istream &strm, Wektor<6> &Wek);
template std::ostream& operator << (std::ostream &strm, const Wektor<6> &Wek);

template std::istream& operator >> (std::istream &strm, Wektor<8> &Wek);
template std::ostream& operator << (std::ostream &strm, const Wektor<8> &Wek);

template drawNS::Point3D convert(const Wektor<3> & arg);