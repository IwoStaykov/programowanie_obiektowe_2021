#include "MacierzRotacji.hh"
#include "Wektor.hh"
#include <vector>
#include <iostream>
#include <cmath>

#define PI 3.14159265359

template <int SIZE>
const Wektor<SIZE> &MatrixRot<SIZE>::operator [] (int index) const{
    if (index < 0 || index >= SIZE){
        std::cerr << "Nieprawidłowy indeks macierzy" << std::endl;
    }
    return row[index];
}

template <int SIZE>
MatrixRot<SIZE>::MatrixRot(){
    Wektor<SIZE> vect;

    if (SIZE != 2 && SIZE != 3){
        std::cerr << "Nieprawidłowy wymiar macierzy" << std::endl;
    }    
    for (int iter=0; iter < SIZE; ++iter){
        row.push_back(vect);
    }
}

/* Przedstawiamy macierz obrotu w 2 lub 3 wymiarach,
   rotation_axis przyjmuje z, x lub y i oznacza oś obrotu.
   Dla 2 wymiaru pomijamy argument dotyczący osi obrotu.   
   Przykładowe wywołanie dla macierzy stopnia 3:
   MatrixRot<3> Macierz(45,'x')                          */
template <int SIZE>
MatrixRot<SIZE>::MatrixRot(double angle_degrees, char rotation_axis){

    Wektor<SIZE> x_row;
    Wektor<SIZE> y_row;
    Wektor<SIZE> z_row;

    double radians = angle_degrees * PI / 180;
    switch(SIZE){
        case 2:
            x_row[0] = cos(radians);
            x_row[1] = -sin(radians);
            y_row[0] = sin(radians);
            y_row[1] = cos(radians);
            row.push_back(x_row);
            row.push_back(y_row);
            break;
        case 3:
            switch (rotation_axis)
            {
            case 'z':
                x_row[0] = cos(radians);
                x_row[1] = -sin(radians);
                y_row[0] = sin(radians);
                y_row[1] = cos(radians);
                z_row[2] = 1;
                break;
            case 'y':
                x_row[0] = cos(radians);
                x_row[2] = sin(radians);
                y_row[1] = 1;
                z_row[0] = -sin(radians);
                z_row[2] = cos(radians);
                break;
            case 'x':
                x_row[0] = 1;
                y_row[1] = cos(radians);
                y_row[2] = -sin(radians);
                z_row[1] = sin(radians);
                z_row[2] = cos(radians);
                break;
            default:
                std::cerr << "Niepoprawna oś obrotu" << std::endl;
                break;
            }
            row.push_back(x_row);
            row.push_back(y_row);
            row.push_back(z_row);
            break;
        default:
            std::cerr << "Niepoprawny wymiar macierzy" << std::endl;
            break;
    }
}
/* Algorytm do mnożenia macierzy*/
template <int SIZE>
MatrixRot<SIZE> MatrixRot<SIZE>::operator * (const MatrixRot<SIZE> &arg) const{
    MatrixRot<SIZE> result;
    double sum;
    for(int horizontal_iter=0; horizontal_iter < SIZE; ++horizontal_iter){
        for(int vertical_iter=0; vertical_iter < SIZE; ++vertical_iter){
            sum = 0;
            for(int iter=0; iter<SIZE; ++iter){
                sum += row[horizontal_iter][iter] * arg[iter][vertical_iter];
            }
            result.row[horizontal_iter][vertical_iter] = sum;
        }
    }
    return result;
}

template <int SIZE>
Wektor<SIZE> MatrixRot<SIZE>::operator * (const Wektor<SIZE> &point) const{
    Wektor<SIZE> result;

    double sum;
    for(int iter=0; iter<SIZE; ++iter){
        sum = 0;
        for(int row_iter=0; row_iter<SIZE; ++row_iter){
            sum += row[iter][row_iter]*point[row_iter];
        }
        result[iter] = sum;
    }
    return result;
}  

template <int SIZE>
std::ostream& operator << (std::ostream &strm, const MatrixRot<SIZE> &Matrix){

    for (int iter=0; iter < SIZE; ++iter){
        strm << Matrix[iter]<<std::endl;
    }
    return strm;
}

template class MatrixRot<2>;
template class MatrixRot<3>;

template std::ostream& operator << (std::ostream &strm, const MatrixRot<2> &Matrix);
template std::ostream& operator << (std::ostream &strm, const MatrixRot<3> &Matrix);
