#include<iostream>
#include<vector>
#include<cmath>
#include "Wektor.hh"
#include "MacierzRotacji.hh"
/* #include "MacierzRotacji.hh"
#include "Prostokat.hh"
#include "Dr3D_gnuplot_api.hh" */

int main()
{
    std::cout.precision(3);
    std::cout.setf(std::ios::fixed);
    using namespace std;
    Wektor<2> x{4,4};

    Wektor<2> y{1,1};
    Wektor<3> z{1,1,1};
    cout << x+y<<endl;
    cout << x-y<<endl;
    cout << y*5<<endl;
    cout << x/2<<endl;

    MatrixRot<2> k(45);
    MatrixRot<3> m(45);
    MatrixRot<3> l(45);
    cout << m << endl;

    MatrixRot<3> h = m*l;
    cout<<h<<endl;
    Wektor<3> w = m*z;
    cout<<w<<endl;

    Wektor<6> w6{1,1,1,1,1,1};
    Wektor<6> w6_2{2,3,2,3,2,3};
    cout << w6 + w6_2<<endl;

/*     MatrixRot<2> M(45); */
/*     cout << M*y; */

/*     MatrixRot<3> R(45,'x');
    cout << R*z;

    MatrixRot<3> Z(45,'y');
    cout << Z*z; */
/*     cout << x+y <<endl; */
    
    
/*     int x; */
/*     Wektor2D ld(1,1);
    Wektor2D lg(1,2);
    Wektor2D pg(3,2);
    Wektor2D pd(3,1);
    Wektor2D mover(2,2); */
    
/*     Wektor2D w(1,1);
    Wektor2D w(2,3);
    MatrixRot Rot(45);
    cout << Rot * w;
    MatrixRot Rot2(50);
    cout << Rot * Rot2;
    cout << w.length(); */
    return 0;
}