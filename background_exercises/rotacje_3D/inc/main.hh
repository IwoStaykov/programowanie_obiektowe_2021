#ifndef MAIN_HH
#define MAIN_HH


/* Struktura przechowująca ostatnią figurę
   Potrzebne dla funkcji "powtórz poprzedni obrót" */
struct RotationSequence
{
    std::vector<char> rotation_axis;
    std::vector<double> degrees;
};


#endif