/* Klasa reprezentuje bryłę (prostopadłościan) i operacje na niej
   do prezentacji w gnuplocie                                    */

#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include<iostream>
#include<vector>
#include<cmath>
#include "Wektor.hh"
#include "MacierzRotacji.hh"
#include "Dr3D_gnuplot_api.hh"

class Figure{
    std::vector<Wektor<3>> coordinates; //4 specyficzne wierzchołki (1 losowy i 3 sąsiadujące)
    int figure_id;
public:
    Figure();
    Figure(Wektor<3> middle_apex, Wektor<3> x_apex, Wektor<3> y_apex, Wektor<3> z_apex);

    void move(const Wektor<3> &vect);
    void rotate(const double &angle_degrees, const char &rotation_axis);
    void draw(drawNS::Draw3DAPI *drafter, std::string colour="black");
    void erase(drawNS::Draw3DAPI *drafter);
    const void equal_sides(int output_info) const;
    Wektor<3> operator [] (int index) const;
};

std::ostream & operator << (std::ostream & strm, const Figure &cube);

#endif