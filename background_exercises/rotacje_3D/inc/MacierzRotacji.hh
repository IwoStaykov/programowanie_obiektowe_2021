#ifndef MACIERZROTACJI_HH
#define MACIERZROTACJI_HH

#include <vector>
#include <iostream>
#include <cmath>
#include "Wektor.hh"

template <int SIZE>
class MatrixRot {
  std::vector<Wektor<SIZE>> row;
public:
  MatrixRot();
  MatrixRot(double angle_degrees, char rotation_axis='z');
  //iloczyn macierzy
  MatrixRot<SIZE> operator * (const MatrixRot<SIZE> &arg) const;
  //iloczyn macierzy i wektora
  Wektor<SIZE> operator * (const Wektor<SIZE> &point) const;
  const Wektor<SIZE> &operator [] (int index) const; //pobranie wartości
};

template <int SIZE>
std::ostream& operator << (std::ostream &strm, const MatrixRot<SIZE> &Matrix);

#endif
