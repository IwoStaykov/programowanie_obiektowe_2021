#ifndef ROTOR_HH
#define ROTOR_HH

#include<iostream>
#include<vector>
#include<cmath>
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "Dr3D_gnuplot_api.hh"
#include "CoordinateSystem.hh"
#include "DrawInterface.hh"

class Rotor : public CoordinateSys, public DrawInterface{
    double radius; //odległość środek podstawy<->wierzchołek podstawy
    double height; // wysokość rotora
    int rotor_id;
    int rotor_blades_id;
public:
    Rotor(Wektor<3> _centre, MatrixRot<3> _orientation, CoordinateSys *_p_previous, double _radius, double _height):
        CoordinateSys(_centre, _orientation, _p_previous), 
        radius(_radius), 
        height(_height) {};
    std::vector<Wektor<3>> calculate();
    void draw(drawNS::Draw3DAPI *drafter) override;
    void draw_blades(drawNS::Draw3DAPI *drafter, std::string colour="blue");
    void erase(drawNS::Draw3DAPI *drafter) override;
    void erase_blades(drawNS::Draw3DAPI *drafter);
};

#endif