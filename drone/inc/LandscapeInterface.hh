#ifndef LANDSCAPEINTERFACE_HH
#define LANDSCAPEINTERFACE_HH

#include "Wektor.hh"

class LandscapeInterface{
public:
    virtual bool drone_above(Wektor<3> drone_centre) = 0; //whether there's space below a drone
    virtual bool can_land() = 0; //should a drone land on the surface
    virtual double get_altitude() = 0; //returns an altitude needed to land a drone
    virtual Wektor<3> get_centre() = 0; //returns a centre of an object
};

#endif