#ifndef INTERDRONE_HH
#define INTERDRONE_HH

class InterDrone{
public:
    virtual void move_vertically(double height) = 0;
    virtual void forward(double distance) = 0;
    virtual void rotate_dron(double degrees) = 0;
    virtual void animate(drawNS::Draw3DAPI *drafter, double height, double distance, double degrees) = 0;
/*     virtual double drone_height() = 0; */
};

#endif