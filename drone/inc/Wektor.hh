#ifndef WEKTOR_HH
#define WEKTOR_HH

#include <iostream>
#include <vector>
#include "Dr3D_gnuplot_api.hh"

template <int SIZE>
class Wektor {
  std::vector<double> vect;
public:
  //przeciążenie indeksów
  const double & operator [] (int index) const; //pobranie wartości
  double & operator [] (int index); //zmiana wartości

  //konstruktory
  Wektor();
  Wektor(std::initializer_list<double> _vect): vect(_vect) {};

  //przeciążenia operatorów do podst operacji na wektorach
  Wektor<SIZE> operator + (const Wektor<SIZE> & second_vect) const;
  Wektor<SIZE> operator - (const Wektor<SIZE> & second_vect) const;
  Wektor<SIZE> operator * (const double scalar) const;
  Wektor<SIZE> operator / (const double scalar) const;
  bool operator == (const Wektor<SIZE> & second_vect) const;
  //iloczyn skalarny
  const double operator * (const Wektor<SIZE> & second_vect) const;
  double length() const;
};

template <int SIZE>
drawNS::Point3D convert(const Wektor<SIZE> & arg);

template <int SIZE>
std::istream& operator >> (std::istream &strm, Wektor<SIZE> &Wek);

template <int SIZE>
std::ostream& operator << (std::ostream &strm, const Wektor<SIZE> &Wek);

#endif
