#ifndef CUBOIDPLANEHILL_HH
#define CUBOIDPLANEHILL_HH

#include "Figure.hh"
#include "LandscapeInterface.hh"
#include "DrawInterface.hh"
#include "CoordinateSystem.hh"
#include "Drone.hh"

class CuboidPlaneHill : public Figure, public LandscapeInterface{
    bool drone_centre_inside;
public:
    CuboidPlaneHill(double _height, double _width, double _length, Wektor<2> _centre={0,0}, MatrixRot<3> _orientation={0, 'z'}, CoordinateSys *_previous=nullptr):
        Figure({_centre[0], _centre[1], -ABS_ALTITUDE+0.5*_height}, _orientation, _previous, _height, _width, _length) {};
    bool drone_above(Wektor<3> drone_centre) override;
    bool can_land() override;
    double get_altitude() override;
    Wektor<3> get_centre() override;
    void draw(drawNS::Draw3DAPI *drafter) override;
};

#endif