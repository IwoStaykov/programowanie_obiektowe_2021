#ifndef DRAWINTERFACE_HH
#define DRAWINTERFACE_HH

#include "Dr3D_gnuplot_api.hh"

class DrawInterface {
public:
    virtual void draw(drawNS::Draw3DAPI *drafter) = 0;
    virtual void erase(drawNS::Draw3DAPI *drafter) = 0;
};

#endif