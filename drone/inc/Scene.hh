#ifndef SCENE_HH
#define SCENE_HH

#include<memory>
#include "DrawInterface.hh"
#include "LandscapeInterface.hh"
#include "Drone.hh"
#include "PlaneHill.hh"
#include "Hill.hh"
#include "InterDrone.hh"
#include "CuboidPlaneHill.hh"

class Scene {
    //shared pointery zrobienia
    std::vector<std::shared_ptr<InterDrone>> drone_collection;
    std::vector<std::shared_ptr<DrawInterface>> drawable_collection;
    std::vector<std::shared_ptr<LandscapeInterface>> obstacle_collection;
    std::shared_ptr<Drone> current;
public:
    Scene();
    void animate_drone(drawNS::Draw3DAPI *drafter, double height=0, double distance=0, double degrees=0);
    void land_drone(drawNS::Draw3DAPI *drafter);
    void draw_all(drawNS::Draw3DAPI *drafter);
    void erase_all(drawNS::Draw3DAPI *drafter);
    void add_drone(Wektor<2> starting_point);
    void choose_drone();
    void remove_drone(int drone_id);
    void add_obstacle(Wektor<2> starting_point, double radius, double height, std::string type="planehill");
    void add_obstacle(Wektor<2> starting_point, double height, double width, double length);
    void remove_obstacle(int obstacle_id);
    void show_drone_ids();
    void show_landscape_ids();
};

#endif