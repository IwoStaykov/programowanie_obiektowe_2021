#ifndef HILL_HH
#define HILL_HH

#include "DrawInterface.hh"
#include "LandscapeInterface.hh"
#include "CoordinateSystem.hh"
#include "Drone.hh"

#define ALTITUDE -20

class Hill : public DrawInterface, public LandscapeInterface, public CoordinateSys{
    double radius;
    double height;
    int random_apex_num;
    int hill_id;
public:
    Hill(double _radius=0, double _height=0, Wektor<2> _centre={0,0}, MatrixRot<3> _orientation={0,'z'}, CoordinateSys *_previous=nullptr);
    std::vector<Wektor<3>> calculate();
    void draw(drawNS::Draw3DAPI *drafter) override;
    void erase(drawNS::Draw3DAPI *drafter) override;
    bool drone_above(Wektor<3> drone_centre) override;
    bool can_land() override;
    double get_altitude() override;
    Wektor<3> get_centre() override;
};

#endif