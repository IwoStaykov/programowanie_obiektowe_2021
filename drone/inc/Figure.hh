/* Klasa reprezentuje bryłę (prostopadłościan) i operacje na niej
   do prezentacji w gnuplocie                                    */

#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include<iostream>
#include<vector>
#include<cmath>
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "Dr3D_gnuplot_api.hh"
#include "CoordinateSystem.hh"
#include "DrawInterface.hh"

class Figure : public CoordinateSys, public DrawInterface{
protected:
    double height;
    double width;
    double length;
    int figure_id;
public:
    Figure(Wektor<3> _centre, MatrixRot<3> _orientation, CoordinateSys *_previous, double _height, double _width, double _length):
        CoordinateSys(_centre, _orientation, _previous),
        height(_height), 
        width(_width), 
        length(_length) {};
    void draw(drawNS::Draw3DAPI *drafter) override;
    void erase(drawNS::Draw3DAPI *drafter) override;
    std::vector<Wektor<3>> calculate();
};

#endif