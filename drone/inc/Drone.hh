#ifndef DRONE_HH
#define DRONE_HH

#include<iostream>
#include<vector>
#include<cmath>
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "Dr3D_gnuplot_api.hh"
#include "CoordinateSystem.hh"
#include "Figure.hh"
#include "Rotor.hh"
#include "InterDrone.hh"
#include "LandscapeInterface.hh"

#define HEIGHT 1
#define WIDTH 4
#define LENGTH 5
#define ROTOR_ROTATE_ANGLE 5
#define FRAMES_NUM 40
#define DELAY 30
#define SPEED 5
#define ABS_ALTITUDE 20

class Drone : protected CoordinateSys, public DrawInterface, public InterDrone, public LandscapeInterface{
    Figure body;
    std::array<Rotor, 4> rotors;
public:
    Drone(Wektor<3> starting_point, MatrixRot<3> _orientation={0,'z'}, CoordinateSys *_p_previous=nullptr) :
        CoordinateSys(starting_point, _orientation, _p_previous),
        body({0,0,0.5}, {0,'z'}, this, HEIGHT, WIDTH, LENGTH),
        rotors{Rotor({-0.5*LENGTH, -0.5*WIDTH, 0.5*HEIGHT+0.25}, {0, 'z'}, this, 1.8, 0.25),
               Rotor({0.5*LENGTH, -0.5*WIDTH, 0.5*HEIGHT+0.25}, {0, 'z'}, this, 1.8, 0.25), 
               Rotor({0.5*LENGTH, 0.5*WIDTH, 0.5*HEIGHT+0.25}, {0, 'z'}, this, 1.8, 0.25), 
               Rotor({-0.5*LENGTH, 0.5*WIDTH, 0.5*HEIGHT+0.25}, {0, 'z'}, this, 1.8, 0.25)} {};
    void draw(drawNS::Draw3DAPI *drafter) override;
    void erase(drawNS::Draw3DAPI *drafter) override;
    void move_vertically(double height) override;
    void forward(double distance) override;
    void rotate_dron(double degrees) override;
    void rotate_rotors();
    void animate(drawNS::Draw3DAPI *drafter, double height=0, double distance=0, double degrees=0) override;
    bool drone_above(Wektor<3> drone_centre) override;
    bool can_land() override;
    Wektor<3> get_centre(){return centre;};
    double get_altitude() override;
};

#endif