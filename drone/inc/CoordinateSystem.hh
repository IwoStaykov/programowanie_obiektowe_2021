#ifndef COORDINATESYSTEM_HH
#define COORDINATESYSTEM_HH

#include"Wektor.hh"
#include"MatrixRot.hh"

class CoordinateSys {
protected:
    Wektor<3> centre;
    MatrixRot<3> orientation;
    CoordinateSys *p_previous;
public:
    CoordinateSys() : centre({0,0,0}), orientation({0,'z'}), p_previous(nullptr) {};
    CoordinateSys(Wektor<3> _centre, MatrixRot<3> _orientation, CoordinateSys *_p_previous=nullptr) : centre(_centre), orientation(_orientation), p_previous(_p_previous){};
    void translate(Wektor<3> mover) { centre = centre + mover; };
    void rotate(MatrixRot<3> matrix) { orientation = orientation * matrix; };
    void calculate_sys_to_global(CoordinateSys & tmp);
    Wektor<3> calculate_point_to_global(Wektor<3> point);
    CoordinateSys *get_previous();
};

#endif
