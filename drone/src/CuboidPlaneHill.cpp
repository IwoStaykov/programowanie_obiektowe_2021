#include "CuboidPlaneHill.hh"

bool CuboidPlaneHill::drone_above(Wektor<3> drone_centre){
    Wektor<2> drone_centre_2D = {drone_centre[0], drone_centre[1]};
    std::vector<Wektor<3>> coordinates_3D = this->calculate();
    std::vector<Wektor<2>> coordinates_2D;
    std::vector<double> coordinates_2D_lengths;
    for(int iter=0; iter < 4; ++iter){
        coordinates_2D.push_back({coordinates_3D[iter][0], coordinates_3D[iter][1]});
    }
    coordinates_2D_lengths.push_back((coordinates_2D[3]-coordinates_2D[0]).length());
    coordinates_2D_lengths.push_back((coordinates_2D[1]-coordinates_2D[0]).length());
    double drone_apex1_distance = (drone_centre_2D - coordinates_2D[0]).length();
    double drone_apex3_distance = (drone_centre_2D - coordinates_2D[2]).length();

    if(drone_apex1_distance + drone_apex3_distance > coordinates_2D_lengths[0] + coordinates_2D_lengths[1]){
        return false;
    }
    std::cout << "Drone is located above a plateu" << std::endl;
    drone_centre_inside = true;
    return true;
}

bool CuboidPlaneHill::can_land(){
    if(!drone_centre_inside){
        return false;
    }
    return true;
}

double CuboidPlaneHill::get_altitude(){
    return height;
}

Wektor<3> CuboidPlaneHill::get_centre(){
    return centre;
}

void CuboidPlaneHill::draw(drawNS::Draw3DAPI *drafter) {
    std::vector<Wektor<3>> coordinates = calculate();
    std::vector<std::vector<drawNS::Point3D>> points; //figure's coordinates passed to the draw_polyhedron function
    std::vector<drawNS::Point3D> first_base;
    std::vector<drawNS::Point3D> second_base;

    //Pierwsza podstawa
    for (int iter=0; iter < 4; ++iter){
        first_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(first_base);

    //Druga podstawa
    for (int iter=4; iter < 8; ++iter){
    second_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(second_base);

    figure_id = drafter->draw_polyhedron(points, "green");
}