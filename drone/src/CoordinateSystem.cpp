#include "Wektor.hh"
#include "MatrixRot.hh"
#include "CoordinateSystem.hh"

void CoordinateSys::calculate_sys_to_global(CoordinateSys & tmp){
    tmp.centre = p_previous->centre + (p_previous->orientation * this->centre);
    tmp.orientation = p_previous->orientation * orientation;
}

/* void CoordinateSys::calculate_point_to_global(CoordinateSys & tmp, Wektor<3> & point){
    point = tmp.centre + (tmp.orientation * point);
} */

Wektor<3> CoordinateSys::calculate_point_to_global(Wektor<3> point){
    Wektor<3> new_point;
    new_point = centre + (orientation * point);
    return new_point;
}

CoordinateSys *CoordinateSys::get_previous(){
    CoordinateSys *grandpa;
    grandpa = p_previous->p_previous;
    return grandpa;
}

