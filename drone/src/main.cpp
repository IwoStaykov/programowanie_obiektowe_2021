#include<iostream>
#include<vector>
#include<cmath>
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "Dr3D_gnuplot_api.hh"
#include "CoordinateSystem.hh"
#include "Figure.hh"
#include "Rotor.hh"
#include "Drone.hh"
#include "DrawInterface.hh"
#include "LandscapeInterface.hh"
#include "PlaneHill.hh"
#include "Hill.hh"
#include "Scene.hh"
#include "CuboidPlaneHill.hh"

void make_surface(drawNS::Draw3DAPI *drafter, std::string colour="black"){
    std::vector<std::vector<drawNS::Point3D>> points;
    std::vector<drawNS::Point3D> lines;
    Wektor<3> first_point;
    Wektor<3> second_point;

    for(double iter=-20; iter < 21; iter += 5){
        first_point = {iter, -20, -20};
        second_point = {iter, 20, -20};
        lines.push_back(convert(first_point));
        lines.push_back(convert(second_point));
        points.push_back(lines);
        drafter->draw_surface(points);
        lines.clear();
        points.clear();
    }
    for(double iter=-20; iter < 21; iter += 5){
        first_point = {-20, iter, -20};
        second_point = {20, iter, -20};
        lines.push_back(convert(first_point));
        lines.push_back(convert(second_point));
        points.push_back(lines);
        drafter->draw_surface(points);
        lines.clear();
        points.clear();
    }
}

void display_menu(){
    using namespace std;
    cout << "=========================   DRON   =========================" << endl;
    cout << "=  o -> drone animation                                    =" << endl;
    cout << "=  a -> add a drone                                        =" << endl;
    cout << "=  r -> remove a drone                                     =" << endl;
    cout << "=  c -> choose a drone                                     =" << endl;
    cout << "=  p -> add a plateu                                       =" << endl;
    cout << "=  k -> add a cuboid plateu                                =" << endl;
    cout << "=  m -> add a mountain                                     =" << endl;
    cout << "=  e -> remove an obstacle                                 =" << endl;
    cout << "=  l -> land drone                                         =" << endl;
    cout << "=  q -> exit                                               =" << endl;
    cout << "============================================================" << endl;
}

int main(){
    drawNS::Draw3DAPI *drafter = new drawNS::APIGnuPlot3D(-20,20,-20,20,-20,20,-1);
    make_surface(drafter);

    Wektor<2> coor1;
    Wektor<2> coor2;
    Wektor<3> new_place;
/*     double up;
    double go;
    double degree; */
    double height;
    double width;
    double length;
    double radius;
    double angle;
    double distance;
    int index;
    Scene world;
    world.draw_all(drafter);
    char input;
    while(input != 'q'){
        display_menu();
        std::cin >> input;
        switch (input)
        {
        case 'o':
            std::cout << "Enter an angle in degrees by which you want to rotate the drone from current position:" << std::endl;
            std::cin >> angle;
            std::cout << "Enter a distance that the drone is supposed to do in a straight line:" << std::endl;
            std::cin >> distance;
            std::cout << "Enter a height that the drone is supposed to reach from current position :" << std::endl;
            std::cin >> height;
            world.animate_drone(drafter, height, distance, angle);
            break;
        case 'a':
            std::cout << "Enter coordinates of a new drone:" <<std::endl;
            std::cin >> coor1;
            world.add_drone(coor1);
            world.draw_all(drafter);
            break;
        case 'r':
            world.show_drone_ids();
            std::cout << "Enter the drone's id:" << std::endl;
            std::cin>>index;
            world.erase_all(drafter);
            world.remove_drone(index);
            world.draw_all(drafter);
            break;
        case 'p':
            std::cout << "Enter coordinates of a new obstacle " << std::endl;
            std::cin >> coor2;
            std::cout << "Enter a height of the new obstacle " << std::endl;
            std::cin >> height;
            std::cout << "Enter a radius of the new obstacle " << std::endl;
            std::cin >> radius;
            world.erase_all(drafter);
            world.add_obstacle(coor2, radius, height, "planehill");
            world.draw_all(drafter);
            break;
        case 'k':
            std::cout << "Enter coordinates of a new obstacle " << std::endl;
            std::cin >> coor2;
            std::cout << "Enter a height of the new obstacle " << std::endl;
            std::cin >> height;
            std::cout << "Enter a width of the new obstacle " << std::endl;
            std::cin >> width;
            std::cout << "Enter a length of the new obstacle " << std::endl;
            std::cin >> length;
            world.erase_all(drafter);
            world.add_obstacle(coor2, height, width, length);
            world.draw_all(drafter);
            break;
        case 'm':
            std::cout << "Enter coordinates of a new obstacle " << std::endl;
            std::cin >> coor2;
            std::cout << "Enter a height of the new obstacle " << std::endl;
            std::cin >> height;
            std::cout << "Enter a radius of the new obstacle " << std::endl;
            std::cin >> radius;
            world.erase_all(drafter);
            world.add_obstacle(coor2,radius,height, "hill");
            world.draw_all(drafter);
            break;
        case 'e':
            world.show_landscape_ids();
            std::cout << "Enter the obstacles's id:" << std::endl;
            std::cin >> index;
            world.erase_all(drafter);
            world.remove_obstacle(index);
            world.draw_all(drafter);
            break;
        case 'c':
            world.choose_drone();
            break;
        case 'l':
            world.land_drone(drafter);
        default:
            break;
        }
    }
    system("pkill gnuplot");
}