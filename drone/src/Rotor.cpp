#include<iostream>
#include<vector>
#include<cmath>
#include "Rotor.hh"
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "Dr3D_gnuplot_api.hh"
#include "CoordinateSystem.hh"
#include "DrawInterface.hh"

std::vector<Wektor<3>> Rotor::calculate(){
    Wektor<3> radius_vect {radius,0,0};
    Wektor<3> height_vect {0,0,height};
    MatrixRot<3> apex_rotation(60, 'z');
    std::vector<Wektor<3>> coordinates;

    Rotor tmp = *this;
    //Find proper centre and orientation in global coordinates system
    while(tmp.p_previous){
        this->calculate_sys_to_global(tmp);
        tmp.p_previous = this->get_previous();//till calculated to global which is nullptr
    }

    //Lower base
    coordinates.push_back(radius_vect - height_vect);
    for(int iter=1; iter < 6; ++iter){
        coordinates.push_back(apex_rotation * coordinates[iter-1]);
    }
    //Upper base
    coordinates.push_back(radius_vect + height_vect);
    for(int iter=7; iter < 12; ++iter){
        coordinates.push_back(apex_rotation * coordinates[iter-1]);
    }
    //Move whole rotor to the proper centre
    for(int iter=0; iter < 12; ++iter){
        coordinates[iter] = tmp.calculate_point_to_global(coordinates[iter]);
    }

    return coordinates;
}

void Rotor::draw(drawNS::Draw3DAPI *drafter){
    std::vector<Wektor<3>> coordinates = calculate();
    std::vector<std::vector<drawNS::Point3D>> points; //prepared coordinates to call draw_polyhedron
    std::vector<drawNS::Point3D> lower_base;
    std::vector<drawNS::Point3D> upper_base;

    this->draw_blades(drafter, "red");

    for(int iter=0; iter < 6; ++iter){
        lower_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(lower_base);

    for(int iter=6; iter < 12; ++iter){
        upper_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(upper_base);

    rotor_id = drafter->draw_polyhedron(points, "red");
}

void Rotor::draw_blades(drawNS::Draw3DAPI *drafter, std::string colour){
    std::vector<Wektor<3>> coordinates = calculate();
    std::vector<drawNS::Point3D> points;

    points.push_back(convert(coordinates[11]));
    points.push_back(convert(coordinates[8]));
    points.push_back(convert(coordinates[9]));
    points.push_back(convert(coordinates[6]));
    points.push_back(convert(coordinates[7]));
    points.push_back(convert(coordinates[10]));

    rotor_blades_id = drafter->draw_polygonal_chain(points, colour);
}

void Rotor::erase_blades(drawNS::Draw3DAPI *drafter){
    drafter->erase_shape(rotor_blades_id);
}

void Rotor::erase(drawNS::Draw3DAPI *drafter){
    this->erase_blades(drafter);
    drafter->erase_shape(rotor_id);
}