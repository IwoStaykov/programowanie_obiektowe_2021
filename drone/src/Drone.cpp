#include "Drone.hh"
#include "Figure.hh"
#include "Rotor.hh"

void Drone::draw(drawNS::Draw3DAPI *drafter){
    body.draw(drafter);
    for(Rotor & iter : rotors){
        iter.draw(drafter);
    }
}

void Drone::erase(drawNS::Draw3DAPI *drafter){
    body.erase(drafter);
    for(Rotor & iter : rotors){
        iter.erase(drafter);
    }
}

void Drone::move_vertically(double height){
    this->translate({0,0,height});
}

void Drone::forward(double distance){
    Wektor<3> centre_frontside{LENGTH/2,0,0}; //Parallel mathematical vector from centre to front side
    double multiplier = distance/centre_frontside.length(); //to move by exact distance
    Wektor<3> mover = orientation * (centre_frontside * multiplier);
    this->translate(mover);
}

void Drone::rotate_dron(double degrees){
    MatrixRot<3> matrix(degrees);
    this->rotate(matrix);
}

void Drone::rotate_rotors(){
    for(Rotor & iter : rotors){
        iter.rotate(ROTOR_ROTATE_ANGLE);
    }
}

void Drone::animate(drawNS::Draw3DAPI *drafter, double height, double distance, double degrees){
/*     double delay_time = height * SPEED; */
    for(int iter=0; iter < FRAMES_NUM; ++iter){
        this->erase(drafter);
        this->move_vertically(height/FRAMES_NUM);
        this->rotate_rotors();
        this->draw(drafter);
        drafter->redraw();
        std::this_thread::sleep_for(std::chrono::milliseconds(DELAY));
    }
/*     delay_time = degrees * SPEED; */
    for(int iter=0; iter < FRAMES_NUM; ++iter){
        this->erase(drafter);
        this->rotate_dron(degrees/FRAMES_NUM);
        this->rotate_rotors();
        this->draw(drafter);
        drafter->redraw();
        std::this_thread::sleep_for(std::chrono::milliseconds(DELAY));
    }
/*     delay_time = distance * SPEED; */
    for(int iter=0; iter < FRAMES_NUM; ++iter){
        this->erase(drafter);
        this->forward(distance/FRAMES_NUM);
        this->rotate_rotors();
        this->draw(drafter);
        drafter->redraw();
        std::this_thread::sleep_for(std::chrono::milliseconds(DELAY));
    }
}

bool Drone::drone_above(Wektor<3> drone_centre){
    Wektor<3> drone_radius{LENGTH/2, WIDTH/2, 0};
    Wektor<2> this_drone = {centre[0], centre[1]};
    Wektor<2> arg_drone = {drone_centre[0], drone_centre[1]};
    double distance = (this_drone - arg_drone).length();
/*     std::cout << distance << std::endl << drone_centre << std::endl << centre << std::endl; */

    if(drone_centre == centre){
        return false;
    }
    if(distance > 3 + 2 * drone_radius.length()){ //add 3 to omit collision of rotors between drones
        return false;
    }
    std::cout << "Dron znajduje się nad innym dronem" << std::endl;
    return true;
}

bool Drone::can_land(){
    return false;
}

double Drone::get_altitude(){
    if (centre[2] < 0){
        return ABS_ALTITUDE - abs(centre[2]);
    }
    return centre[2] + ABS_ALTITUDE;
}

/* bool Drone::drone_above(std::shared_ptr<Drone> drone){
    Wektor<3> drone_centre = drone->get_centre();
    Wektor<3> drone_radius{LENGTH/2, WIDTH/2, 0};
    double distance = (drone_centre - centre).length();

    if(distance > 2 * drone_radius.length()){
        return false;
    }
    std::cout << "Dron znajduje się nad innym dronem" << std::endl;
    return true;
}

bool Drone::can_land(){
    return false;
} */

/* double Drone::get_altitude(){
    return 10;
}

bool Drone::drone_above(){
    return true;
}

bool Drone::can_land(){
    return false;
} */