#include "Scene.hh"
#include<memory>
#include<algorithm>
#include "DrawInterface.hh"
#include "Drone.hh"

Scene::Scene(){
    obstacle_collection.push_back(std::shared_ptr<LandscapeInterface> (new Hill(10,10,{-12,12})));
    drawable_collection.push_back(std::static_pointer_cast<Hill> (*(obstacle_collection.end()-1)));
    obstacle_collection.push_back(std::shared_ptr<LandscapeInterface> (new Hill(5,14,{0,5})));
    drawable_collection.push_back(std::static_pointer_cast<Hill> (*(obstacle_collection.end()-1)));
    obstacle_collection.push_back(std::shared_ptr<LandscapeInterface> (new PlaneHill(7,10,{10,15})));
    drawable_collection.push_back(std::static_pointer_cast<PlaneHill> (*(obstacle_collection.end()-1)));
    obstacle_collection.push_back(std::shared_ptr<LandscapeInterface> (new CuboidPlaneHill(5,8,10,{15,-15})));
    drawable_collection.push_back(std::static_pointer_cast<CuboidPlaneHill> (*(obstacle_collection.end()-1)));
    drone_collection.push_back(std::shared_ptr<InterDrone> (new Drone({-10,-17,-20})));
    drawable_collection.push_back(std::static_pointer_cast<Drone> (*(drone_collection.end()-1)));
    drone_collection.push_back(std::shared_ptr<InterDrone> (new Drone({-10,-8,-20})));
    drawable_collection.push_back(std::static_pointer_cast<Drone> (*(drone_collection.end()-1)));
    current = std::dynamic_pointer_cast<Drone> (drone_collection[drone_collection.size() - 1]);
}

void Scene::draw_all(drawNS::Draw3DAPI *drafter){
    //Erase all drones to omit drawing them twice
    std::shared_ptr<Drone> drone;
    for(uint iter=0; iter < drone_collection.size(); ++iter){
        drone = std::dynamic_pointer_cast<Drone> (drone_collection[iter]);
        drone->erase(drafter);
    }
    for(uint iter=0; iter < drawable_collection.size(); ++iter){
        drawable_collection[iter]->draw(drafter);
        drafter->redraw();
    }
}

void Scene::erase_all(drawNS::Draw3DAPI *drafter){

/*     std::shared_ptr<DrawInterface> drawer; */
    for (uint iter = 0; iter < drawable_collection.size(); ++iter){
/*         drawer = std::dynamic_pointer_cast<DrawInterface> (drawable_collection[iter]); */
        drawable_collection[iter]->erase(drafter);
    } 
}

void Scene::remove_obstacle(int obstacle_id){
    
    //Remove from obstacle_collection
    std::shared_ptr<LandscapeInterface> tmp_obstacle = obstacle_collection[obstacle_id];
    obstacle_collection.erase(std::remove(obstacle_collection.begin(),obstacle_collection.end(),tmp_obstacle), obstacle_collection.end());
    //Remove from drawable_collection
    std::shared_ptr<DrawInterface> tmp_drawable = std::dynamic_pointer_cast<DrawInterface> (tmp_obstacle);
    drawable_collection.erase(std::remove(drawable_collection.begin(),drawable_collection.end(),tmp_drawable), drawable_collection.end());
}

void Scene::animate_drone(drawNS::Draw3DAPI *drafter, double height, double distance, double degrees){
    current->animate(drafter, height, distance, degrees);
}

void Scene::land_drone(drawNS::Draw3DAPI *drafter){
    std::shared_ptr<Drone> drone;
    Wektor<3> drone_centre;
    bool touch_down = false;
    double height;
    for(uint iter=1; iter < drone_collection.size(); ++iter){
        drone = std::dynamic_pointer_cast<Drone> (drone_collection[iter]);
        while(current->drone_above(drone->get_centre())){
            this->animate_drone(drafter, 0, 3, 0);
        }
    }
    for(uint iter=0; iter < obstacle_collection.size(); ++iter){
        while(obstacle_collection[iter]->drone_above(current->get_centre())){
            for(uint iter=1; iter < drone_collection.size(); ++iter){
                drone = std::dynamic_pointer_cast<Drone> (drone_collection[iter]);
                while(current->drone_above(drone->get_centre())){
                    this->animate_drone(drafter, 0, 3, 0);
                }
            }
            if(obstacle_collection[iter]->can_land()){
                drone_centre = current->get_centre();
                height = abs(current->get_altitude()) - abs(obstacle_collection[iter]->get_altitude());
                this->animate_drone(drafter, -height);
                std::cout << "Dron wylądował na płaskowyżu" << std::endl;
                touch_down = true;
                break;
            }
            std::cout << "Wykryto przeszkodę, automatyczne wyszukiwanie miejsca do lądowania" << obstacle_collection[iter]->can_land() << std::endl;
            this->animate_drone(drafter, 0, 3, 0);
        }
    }
    if(!touch_down){
        this->animate_drone(drafter, -abs(current->get_altitude()));
        std::cout << "Dron wylądował na ziemi" << std::endl;
    }
}


void Scene::add_drone(Wektor<2> starting_point){
    drone_collection.push_back(std::shared_ptr<InterDrone> (new Drone({starting_point[0],starting_point[1], ALTITUDE})));
    drawable_collection.push_back(std::dynamic_pointer_cast<DrawInterface> (*(drone_collection.end()-1)));
    current = std::dynamic_pointer_cast<Drone> (drone_collection[drone_collection.size()-1]);
}

void Scene::remove_drone(int drone_id){
    //Remove from drone_collection
    std::shared_ptr<InterDrone> tmp_drone = drone_collection[drone_id];
    drone_collection.erase((std::remove(drone_collection.begin(),drone_collection.end(),tmp_drone)), drone_collection.end());
    //Remove from drawable_collection
    std::shared_ptr<DrawInterface> tmp_drawable = std::static_pointer_cast<Drone> (std::static_pointer_cast<Drone>(tmp_drone));
    drawable_collection.erase(std::remove(drawable_collection.begin(),drawable_collection.end(),tmp_drawable), drawable_collection.end());

    current = std::dynamic_pointer_cast<Drone> (drone_collection[drone_collection.size()-1]);
}

void Scene::add_obstacle(Wektor<2> starting_point, double radius, double height, std::string type){
    if(type == "planehill"){
        obstacle_collection.push_back(std::shared_ptr<PlaneHill> (new PlaneHill(radius, height, starting_point)));
        drawable_collection.push_back(std::dynamic_pointer_cast<DrawInterface> (*(obstacle_collection.end()-1)));
    }
    else if (type == "hill"){
        obstacle_collection.push_back(std::shared_ptr<Hill> (new Hill(radius, height, starting_point)));
        drawable_collection.push_back(std::dynamic_pointer_cast<DrawInterface> (*(obstacle_collection.end()-1)));
    }
    else{
        std::cerr << "Zły typ przeszkody do utworzenia." << std::endl;
    }
}

void Scene::add_obstacle(Wektor<2> starting_point, double height, double width, double length){
    obstacle_collection.push_back(std::shared_ptr<CuboidPlaneHill> (new CuboidPlaneHill(height, width, length, starting_point)));
    drawable_collection.push_back(std::dynamic_pointer_cast<DrawInterface> (*(obstacle_collection.end()-1)));
}


void Scene::show_drone_ids(){
    std::cout << "Aktualnie dostępne drony:" << std::endl;
    std::shared_ptr<Drone> drone;
    for(uint iter=0; iter < drone_collection.size(); ++iter){
        drone = std::dynamic_pointer_cast<Drone> (drone_collection[iter]);
        std::cout <<  "Numer id drona:    " << iter <<"    Lokalizacja drona    " << drone->get_centre() << std::endl;
    }
}

void Scene::show_landscape_ids(){
    std::cout << "Aktualnie istniejące przeszkody:" << std::endl;
    for (uint iter=0; iter < obstacle_collection.size(); ++iter){
        std::cout << "Numer id przeszkody:    " << iter << "    Lokalizacja przeszkody:    " << obstacle_collection[iter]->get_centre() << std::endl;        
    }
}

void Scene::choose_drone(){
    this->show_drone_ids();
    std::cout << "Wybierz id nowego aktualnego drona:" << std::endl;
    uint user_input;
    std::cin >> user_input;
    while(user_input >= drone_collection.size() || user_input < 0){
        std::cout << "Wprowadzono niepoprawny indeks drona" << std::endl;
        std::cin >> user_input;
    }
    current = std::dynamic_pointer_cast<Drone> (drone_collection[user_input]);
    std::cout << "Przydzielono kontrolę nad dronem nr " << user_input << std::endl;
}


