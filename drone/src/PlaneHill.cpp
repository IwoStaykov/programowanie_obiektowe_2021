#include<random>
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "PlaneHill.hh"

#define ROUND_ANGLE 360

PlaneHill::PlaneHill(double _radius, double _height, Wektor<2> _centre, MatrixRot<3> _orientation, CoordinateSys *_previous){
    this->centre = {_centre[0],_centre[1], ALTITUDE+0.5*_height};
    this->orientation = _orientation;
    this->p_previous = _previous;
    radius = _radius;
    height = 0.5*_height;

    //Random number of plane hill apexes
    std::random_device randomizer;
    std::default_random_engine generator(randomizer());
    std::uniform_int_distribution<int> distribution(3,9);
    random_apex_num = distribution(generator);
}

std::vector<Wektor<3>> PlaneHill::calculate(){
    Wektor<3> radius_vect {radius,0,0};
    Wektor<3> height_vect {0,0,height};
    MatrixRot<3> apex_rotation(ROUND_ANGLE/random_apex_num, 'z');
    std::vector<Wektor<3>> coordinates;

    //Lower base
    coordinates.push_back(radius_vect - height_vect);
    for(int iter=1; iter < random_apex_num; ++iter){
        coordinates.push_back(apex_rotation * coordinates[iter-1]);
    }
    //Upper base
    coordinates.push_back(radius_vect + height_vect);
    for(int iter=random_apex_num+1; iter < random_apex_num*2; ++iter){
        coordinates.push_back(apex_rotation * coordinates[iter-1]);
    }
    //Move a figure to the proper place with the proper orientation
    for(int iter=0; iter < random_apex_num*2; ++iter){
        coordinates[iter] = this->calculate_point_to_global(coordinates[iter]);
    }

    return coordinates;
}

void PlaneHill::draw(drawNS::Draw3DAPI *drafter){
    std::vector<Wektor<3>> coordinates = calculate();
    std::vector<std::vector<drawNS::Point3D>> points;
    std::vector<drawNS::Point3D> lower_base;
    std::vector<drawNS::Point3D> upper_base;

    for(int iter=0; iter < random_apex_num; ++iter){
        lower_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(lower_base);

    for(int iter=random_apex_num; iter < random_apex_num*2; ++iter){
        upper_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(upper_base);

    planehill_id = drafter->draw_polyhedron(points, "green");
    
}

void PlaneHill::erase(drawNS::Draw3DAPI *drafter){
    drafter->erase_shape(planehill_id);
}

bool PlaneHill::drone_above(Wektor<3> drone_centre){
    Wektor<3> drone_radius{LENGTH/2, WIDTH/2, 0};
    Wektor<2> planehill = {centre[0], centre[1]};
    Wektor<2> drone = {drone_centre[0], drone_centre[1]};
    double distance = (drone - planehill).length();
    if(distance > radius + drone_radius.length()){
        return false;
    }
    std::cout << "Dron znajduje się nad płaskowyżem" << std::endl;
    return true;
}

bool PlaneHill::can_land(){
    return true;
}

double PlaneHill::get_altitude(){
    return 2*height;
}

Wektor<3> PlaneHill::get_centre(){
    return centre;
}

/* bool PlaneHill::drone_above(std::shared_ptr<Drone> drone){
    Wektor<3> drone_centre = drone->get_centre();
    Wektor<3> drone_radius{LENGTH/2, WIDTH/2, 0};
    double distance = (drone_centre - centre).length();

    if(distance > radius + drone_radius.length()){
        return false;
    }
    std::cout << "Dron znajduje się nad płaskowyżem" << std::endl;
    return true;
}

bool PlaneHill::can_land(){
    return true;
} */

/* double PlaneHill::get_altitude(){
    return height + ALTITUDE;
} */