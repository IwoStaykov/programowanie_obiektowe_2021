#include <iostream>
#include <vector>
#include <cmath>
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "Figure.hh"

#define NUM_APEXES 8

std::vector<Wektor<3>> Figure::calculate(){
    //Mathematical vectors from centre to apexes
    //Lower base
    Wektor<3> apex1 = {-0.5*length, -0.5*width, -0.5*height};
    Wektor<3> apex2 = {0.5*length, -0.5*width, -0.5*height};
    Wektor<3> apex3 = {0.5*length, 0.5*width, -0.5*height};
    Wektor<3> apex4 = {-0.5*length, 0.5*width, -0.5*height};
    //Upper base
    Wektor<3> apex5 = {-0.5*length, -0.5*width, 0.5*height};
    Wektor<3> apex6 = {0.5*length, -0.5*width, 0.5*height};
    Wektor<3> apex7 = {0.5*length, 0.5*width, 0.5*height};
    Wektor<3> apex8 = {-0.5*length, 0.5*width, 0.5*height};
    std::vector<Wektor<3>> content {apex1, apex2, apex3, apex4, apex5, apex6, apex7, apex8};

    Figure tmp = *this; //not to loose pointer to parent
    //Find proper centre and orientation in global coordinates system
    while(tmp.p_previous){
        this->calculate_sys_to_global(tmp);
        tmp.p_previous = this->get_previous(); //till calculated to global which is nullptr
    }

    //Calculate apexes for that centre and orientation
    std::vector<Wektor<3>> coordinates;
    for (int iter=0; iter < NUM_APEXES; ++iter){
        coordinates.push_back(tmp.centre + (tmp.orientation * content[iter]));
/*         coordinates.push_back(this->calculate_point_to_global(content[iter])); */
    }
    
    return coordinates;
}

void Figure::draw(drawNS::Draw3DAPI *drafter) {
    std::vector<Wektor<3>> coordinates = calculate();
    std::vector<std::vector<drawNS::Point3D>> points; //figure's coordinates passed to the draw_polyhedron function
    std::vector<drawNS::Point3D> first_base;
    std::vector<drawNS::Point3D> second_base;

    //First base
    for (int iter=0; iter < 4; ++iter){
        first_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(first_base);

    //Second base
    for (int iter=4; iter < 8; ++iter){
    second_base.push_back(convert(coordinates[iter]));
    }
    points.push_back(second_base);

    figure_id = drafter->draw_polyhedron(points, "blue");
}

void Figure::erase(drawNS::Draw3DAPI *drafter){
    drafter->erase_shape(figure_id);
}