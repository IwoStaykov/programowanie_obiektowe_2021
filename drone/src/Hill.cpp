#include<iostream>
#include<random>
#include "Wektor.hh"
#include "MatrixRot.hh"
#include "Hill.hh"

#define ROUND_ANGLE 360

Hill::Hill(double _radius, double _height, Wektor<2> _centre, MatrixRot<3> _orientation, CoordinateSys *_previous){
    this->centre = {_centre[0],_centre[1], ALTITUDE+0.5*_height};
    this->orientation = _orientation;
    this->p_previous = _previous;
    radius = _radius;
    height = 0.5*_height;

    //Random number of hill apexes
    std::random_device randomizer;
    std::default_random_engine generator(randomizer());
    std::uniform_int_distribution<int> distribution(3,9);
    random_apex_num = distribution(generator);
}

std::vector<Wektor<3>> Hill::calculate(){
    Wektor<3> radius_vect {radius,0,0};
    Wektor<3> height_vect {0,0,height};
    MatrixRot<3> apex_rotation(ROUND_ANGLE/random_apex_num, 'z');
    std::vector<Wektor<3>> coordinates;

    //Lower base
    coordinates.push_back(radius_vect - height_vect);
    for(int iter=0; iter < random_apex_num; ++iter){
        coordinates.push_back(apex_rotation * coordinates[iter]);
    }
    //Upper apex
    coordinates.push_back(height_vect);
    
    //Move hill to the proper place and proper orientation
    for(int iter=0; iter < random_apex_num+2; ++iter){
        coordinates[iter] = this->calculate_point_to_global(coordinates[iter]);
    }

    return coordinates;
}

void Hill::draw(drawNS::Draw3DAPI *drafter){
    std::vector<Wektor<3>> coordinates = calculate();
    std::vector<std::vector<drawNS::Point3D>> points;
    std::vector<drawNS::Point3D> lower_base;
    std::vector<drawNS::Point3D> upper_apex;

    for(int iter=0; iter < random_apex_num; ++iter){
        lower_base.push_back(convert(coordinates[iter]));
    }
    for(int iter=0; iter < random_apex_num; ++iter){
        upper_apex.push_back(convert(coordinates[random_apex_num+1]));
    }

    points.push_back(lower_base);
    points.push_back(upper_apex);

    hill_id = drafter->draw_polyhedron(points, "green");
}

void Hill::erase(drawNS::Draw3DAPI *drafter){
    drafter->erase_shape(hill_id);
}

bool Hill::drone_above(Wektor<3> drone_centre){
    Wektor<3> drone_radius{LENGTH/2, WIDTH/2, 0};
    Wektor<2> hill = {centre[0], centre[1]};
    Wektor<2> drone = {drone_centre[0], drone_centre[1]};
    double distance = (drone - hill).length();

    if(distance > radius + drone_radius.length()){
        return false;
    }
    std::cout << "Dron znajduje się nad wzgórzem" << std::endl;
    return true;
}

bool Hill::can_land(){
    return false;
}

double Hill::get_altitude(){
    return height;
}

Wektor<3> Hill::get_centre(){
    return centre;
}

/* bool Hill::drone_above(std::shared_ptr<Drone> drone){
    Wektor<3> drone_centre = drone->get_centre();
    Wektor<3> drone_radius{LENGTH/2, WIDTH/2, 0};
    double distance = (drone_centre - centre).length();

    if(distance > radius + drone_radius.length()){
        return false;
    }
    std::cout << "Dron znajduje się nad wzgórzem" << std::endl;
    return true;
}

bool Hill::can_land(){
    return false;
} */

/* double Hill::get_altitude(){
    return height + ALTITUDE;
} */